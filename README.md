# Water Simulation

## To build

```
meson builddir
ninja -C builddir
``` 

## To run

```
cp -R shaders builddir/
builddir/WaterSimulation
``` 