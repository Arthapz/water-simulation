// Copyright (C) 2019 Arthur LAURENT <arthur.laurent4@gmail.com>
// This file is subject to the license terms in the LICENSE file
// found in the top-level of this distribution

#include "Scene.hpp"
#include "Map/Map.hpp"
#include "Map/Water.hpp"

#include <glm/gtc/noise.hpp>

/////////// - StormKit::render - ///////////
#include <storm/render/core/Device.hpp>
#include <storm/render/core/PhysicalDevice.hpp>
#include <storm/render/core/Surface.hpp>

/////////// - StormKit::engine - ///////////
#include <storm/engine/Engine.hpp>

#include <storm/engine/core/Transform.hpp>

#include <storm/engine/material/Material.hpp>

#include <storm/engine/drawable/3D/CubeMap.hpp>

#include <storm/engine/scene/FPSCamera.hpp>

#include <storm/engine/framegraph/FrameGraph.hpp>
#include <storm/engine/framegraph/FramePass.hpp>
#include <storm/engine/framegraph/FramePassBuilder.hpp>
#include <storm/engine/framegraph/FramePassResources.hpp>

using namespace storm;
using storm::log::operator""_module;

static constexpr auto LOG_MODULE = "WaterSimulation"_module;

////////////////////////////////////////
////////////////////////////////////////
Scene::Scene(engine::Engine &engine,
                     const window::Window &window)
    : engine::PBRScene { engine }, m_window { &window }, m_input_handler { window } {
    const auto &device = m_engine->device();

    m_camera = std::make_unique<engine::FPSCamera>(
        *m_engine,
        m_engine->surface().extent().convertTo<core::Extentf>(), 0.1f, 1000.f);
    m_camera->setPosition({ 0.f, 0.f, -0.5f });
    m_camera->setRotation({ 0.f, 0.f, 0.f });
    m_camera->setMoveSpeed({40.f, 40.f, 40.f});

    setCamera(*m_camera);

    m_engine->debugGUI().setSkipFrameCount(40);

    m_cube_map = std::make_unique<engine::CubeMap>(*this);

    auto image = image::Image { "textures/cubemap.ktx" };

    auto &cube_map_texture = texturePool().create("CubeMap",
                                                  m_engine->device(),
                                                  image.extent(),
                                                  render::PixelFormat::RGBA16F,
                                                  1u,
                                                  12u,
                                                  render::TextureType::T2D,
                                                  render::TextureCreateFlag::Cube_Compatible);
    cube_map_texture.loadFromImage(image);

    m_cube_map->setTexture(cube_map_texture);

    auto heightmap = image::Image{ "textures/heightmap.png" };

    auto &map_material = *m_material_pool.create("MapMaterial", std::make_unique<MapMaterial>(*this));
    auto &water_material = *m_material_pool.create("WaterMaterial", std::make_unique<WaterMaterial>(*this));

    auto grass_image = image::Image{ "textures/grass.jpg" }.toFormat(image::Image::Format::RGBA8_UNorm);
    const auto grass_mip_levels = render::computeMipLevel(grass_image.extent());

    auto &grass_texture = m_texture_pool.create("grass",
                                        device,
                                        grass_image.extent(),
                                        render::PixelFormat::RGBA8_UNorm,
                                        1u,
                                        grass_mip_levels,
                                        render::TextureType::T2D);
    grass_texture.loadFromImage(grass_image, true);

    auto dirt_image = image::Image{ "textures/dirt.jpg" }.toFormat(image::Image::Format::RGBA8_UNorm);
    const auto dirt_mip_levels = render::computeMipLevel(dirt_image.extent());

    auto &dirt_texture = m_texture_pool.create("dirt",
                                        device,
                                        dirt_image.extent(),
                                        render::PixelFormat::RGBA8_UNorm,
                                        1u,
                                        dirt_mip_levels,
                                        render::TextureType::T2D);
    dirt_texture.loadFromImage(dirt_image, true);

    m_map = std::make_unique<Map>(*m_engine, map_material, heightmap, 4.f);
    m_map->materialInstance().setGrassMap(grass_texture);
    m_map->materialInstance().setDirtMap(dirt_texture);

    m_water = std::make_unique<Water>(*m_engine, water_material, heightmap.extent().width, heightmap.extent().height, 4.f);

    enableDepthTest(true);
    toggleMSAA();
}

////////////////////////////////////////
////////////////////////////////////////
Scene::~Scene() = default;

////////////////////////////////////////
////////////////////////////////////////
Scene::Scene(Scene &&) = default;

////////////////////////////////////////
////////////////////////////////////////
Scene &Scene::operator=(Scene &&) = default;

////////////////////////////////////////
////////////////////////////////////////
void Scene::toggleWireframe() {
    const auto &capabilities = m_engine->device().physicalDevice().capabilities();
    if (!capabilities.features.fill_Mode_non_solid) {
        log::LogHandler::elog(LOG_MODULE, "Wireframe is not supported on this GPU");
        return;
    }

    m_wireframe = !m_wireframe;
}

////////////////////////////////////////
////////////////////////////////////////
void Scene::toggleMSAA() noexcept {
    if (m_engine->maxSampleCount() != render::SampleCountFlag::C1_BIT) {
        if (m_sample_count == m_engine->maxSampleCount())
            m_sample_count = render::SampleCountFlag::C1_BIT;

        m_sample_count = core::nextValue(m_sample_count);
    }

    setMSAASampleCount(m_sample_count);
}

////////////////////////////////////////
////////////////////////////////////////
void Scene::update(float delta) {
    const auto extent = m_engine->surface().extent().convertTo<core::Extenti>();

    if (!m_freeze_camera) {
        auto camera_inputs = engine::FPSCamera::Inputs {};

        if (m_input_handler.isKeyPressed(window::Key::Z)) camera_inputs.up = true;
        if (m_input_handler.isKeyPressed(window::Key::S)) camera_inputs.down = true;
        if (m_input_handler.isKeyPressed(window::Key::Q)) camera_inputs.left = true;
        if (m_input_handler.isKeyPressed(window::Key::D)) camera_inputs.right = true;

        const auto position = [&camera_inputs, &extent, this]() {
            auto position = m_input_handler.getMousePositionOnWindow();

            if (position->x <= 5 || position->x > (extent.w - 5)) {
                position->x                = extent.w / 2;
                camera_inputs.mouse_ignore = true;
            }
            if (position->y <= 5 || position->y > (extent.h - 5)) {
                position->y                = extent.h / 2;
                camera_inputs.mouse_ignore = true;
            }

            m_input_handler.setMousePositionOnWindow(position);

            return position;
        }();

        camera_inputs.mouse_updated = true;
        camera_inputs.x_mouse       = static_cast<float>(position->x);
        camera_inputs.y_mouse       = static_cast<float>(position->y);

        m_camera->feedInputs(camera_inputs);
    }

    if (m_show_debug_gui) {
        auto &debug_gui = m_engine->debugGUI();
        debug_gui.update(*m_window);
    }

    m_delta_time = delta;
}

////////////////////////////////////////
////////////////////////////////////////
void Scene::doRenderScene(storm::engine::FrameGraph &framegraph,
                              storm::engine::FramePassTextureID backbuffer,
                              std::vector<storm::engine::BindableBaseConstObserverPtr> bindables,
                              storm::render::GraphicsPipelineState &state) {
    const auto &surface = m_engine->surface();

    if (m_cube_map_dirty) {
        insertGenerateCubeMapPass(framegraph, *m_cube_map);
        m_cube_map_dirty = false;
    }

    bindables.emplace_back(core::makeConstObserver(m_data));

    struct ColorPrePassData {
        engine::FramePassTextureID output;
    };
    const auto pre_pass_output_descriptor =
        engine::FrameGraphTexture::Descriptor { .type    = render::TextureType::T2D,
                                                .format  = surface.pixelFormat(),
                                                .extent  = surface.extent(),
                                                .samples = m_sample_count,
                                                .usage   = render::TextureUsage::Color_Attachment };

    auto &color_pre_pass =
        framegraph.addPass<ColorPrePassData>(
             "ColorPrePass",
             [&pre_pass_output_descriptor](engine::FramePassBuilder &builder, ColorPrePassData &pass_data) {
                 pass_data.output = builder.create<engine::FrameGraphTexture>("prepass_output", std::move(pre_pass_output_descriptor));
                 pass_data.output = builder.write(pass_data.output);
             },
            [this, bindables, state]([[maybe_unused]] const ColorPrePassData &pass_data,
                                                      const engine::FramePassResources &resources,
                                                      render::CommandBuffer &cmb) {
                auto mesh_state = state;

                mesh_state.color_blend_state.attachments = {
                    render::GraphicsPipelineColorBlendAttachmentState {}
                };

                m_map->render(cmb, resources.renderPass(), bindables, mesh_state);
            });
    color_pre_pass.setCullImune(true);


    struct ColorPassData {
        //engine::FramePassTextureID prepass_input;

        engine::FramePassTextureID depth;
        engine::FramePassTextureID msaa;
        engine::FramePassTextureID output;
    };

    const auto depth_descriptor = engine::FrameGraphTexture::Descriptor {
        .type    = render::TextureType::T2D,
        .format  = render::PixelFormat::Depth32F_Stencil8,
        .extent  = surface.extent(),
        .samples = m_sample_count,
        .usage   = render::TextureUsage::Depth_Stencil_Attachment
    };
    const auto msaa_descriptor =
        engine::FrameGraphTexture::Descriptor { .type    = render::TextureType::T2D,
                                                .format  = surface.pixelFormat(),
                                                .extent  = surface.extent(),
                                                .samples = m_sample_count,
                                                .usage   = render::TextureUsage::Color_Attachment };


    const auto setup =
        [&backbuffer, &depth_descriptor, &msaa_descriptor, &color_pre_pass, this](engine::FramePassBuilder &builder,
                                                                 ColorPassData &pass_data) {
            //pass_data.prepass_input = builder.read(color_pre_pass.data().output);
            pass_data.depth =
                builder.create<engine::FrameGraphTexture>("Depth", std::move(depth_descriptor));

            if (isMSAAEnabled()) {
                pass_data.msaa =
                    builder.create<engine::FrameGraphTexture>("MSAA", std::move(msaa_descriptor));
                pass_data.msaa  = builder.write(pass_data.msaa);
                pass_data.depth = builder.write(pass_data.depth);

                pass_data.output = builder.resolve(backbuffer);
            } else {
                pass_data.output = builder.write(backbuffer);
                pass_data.depth  = builder.write(pass_data.depth);
            }
        };

    const auto execute = [this, bindables, state]([[maybe_unused]] const ColorPassData &pass_data,
                                                  const engine::FramePassResources &resources,
                                                  render::CommandBuffer &cmb) {
        auto mesh_state = state;

        mesh_state.color_blend_state.attachments = {
            render::GraphicsPipelineColorBlendAttachmentState {}
        };

        m_cube_map->render(cmb, resources.renderPass(), bindables, mesh_state);
        m_map->render(cmb, resources.renderPass(), bindables, mesh_state);
        m_water->render(cmb, resources.renderPass(), bindables, mesh_state);
    };

    auto &color_pass =
        framegraph.addPass<ColorPassData>("ColorPass", std::move(setup), std::move(execute));

    color_pass.setCullImune(true);

    if (m_show_debug_gui) m_engine->doInitDebugGUIPasses(color_pass.data().output, framegraph);
}
