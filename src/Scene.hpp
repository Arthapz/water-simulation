#pragma once

/////////// - StormKit::window - ///////////
#include <storm/window/InputHandler.hpp>

/////////// - StormKit::core - ///////////
#include <storm/render/core/Enums.hpp>

/////////// - StormKit::engine - ///////////
#include <storm/engine/Fwd.hpp>

#include <storm/engine/scene/PBRScene.hpp>

#include <storm/engine/material/PBRMaterialInstance.hpp>

class Map;
class Water;
class Scene final: public storm::engine::PBRScene {
  public:
    explicit Scene(storm::engine::Engine &engine,
                       const storm::window::Window &window);
    ~Scene() override;

    Scene(Scene &&);
    Scene &operator=(Scene &&);

    void toggleWireframe();
    void toggleMSAA() noexcept;
    inline void toggleFreezeCamera() noexcept { m_freeze_camera = !m_freeze_camera; }
    inline void toggleDebugGUI() noexcept { m_show_debug_gui = !m_show_debug_gui; }

    void update(float time);

  protected:
    void doRenderScene(storm::engine::FrameGraph &framegraph,
                       storm::engine::FramePassTextureID backbuffer,
                       std::vector<storm::engine::BindableBaseConstObserverPtr> bindables,
                       storm::render::GraphicsPipelineState &state) override;

  private:
    storm::window::WindowConstObserverPtr m_window;

    storm::engine::FPSCameraOwnedPtr m_camera;

    storm::window::InputHandler m_input_handler;

    float m_delta_time = 0.f;

    bool m_freeze_camera  = false;
    bool m_wireframe      = false;
    bool m_show_debug_gui = false;

    std::unique_ptr<Map> m_map;
    std::unique_ptr<Water> m_water;

    storm::render::SampleCountFlag m_sample_count = storm::render::SampleCountFlag::C1_BIT;
};
