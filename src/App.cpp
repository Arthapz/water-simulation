// Copyright (C) 2019 Arthur LAURENT <arthur.laurent4@gmail.com>
// This file is subject to the license terms in the LICENSE file
// found in the top-level of this distribution

#include "App.hpp"
#include "Scene.hpp"


/////////// - StormKit::log - ///////////
#include <storm/log/LogHandler.hpp>

/////////// - StormKit::window - ///////////
#include <storm/window/EventHandler.hpp>
#include <storm/window/Window.hpp>

/////////// - StormKit::render - ///////////
#include <storm/render/core/CommandBuffer.hpp>
#include <storm/render/core/Device.hpp>
#include <storm/render/core/PhysicalDevice.hpp>
#include <storm/render/core/Surface.hpp>

/////////// - StormKit::engine - ///////////
#include <storm/engine/Engine.hpp>

using namespace storm;
using storm::log::operator""_module;

template<typename T>
static constexpr auto WINDOW_WIDTH = T { 1280 };
template<typename T>
static constexpr auto WINDOW_HEIGHT = T { 768 };
static constexpr auto WINDOW_TITLE  = "WaterSimulation";
static constexpr auto LOG_MODULE    = "WaterSimulation"_module;

App::App() {
    log::LogHandler::ilog(LOG_MODULE,
                          "Using StormKit {}.{}.{} {} {}",
                          STORM_MAJOR_VERSION,
                          STORM_MINOR_VERSION,
                          STORM_PATCH_VERSION,
                          STORM_GIT_BRANCH,
                          STORM_GIT_COMMIT_HASH);
}

App::~App() = default;

void App::run([[maybe_unused]] int argc, [[maybe_unused]] char **argv) {
    for (auto i = 1; i < argc; ++i) {
        if (std::string { argv[i] } == std::string { "--fullscreen" })
            m_fullscreen = true;
    }

    doInitWindow();

    namespace Chrono = std::chrono;
    using Clock      = std::chrono::high_resolution_clock;

    auto input_handler = window::InputHandler { *m_window };

    input_handler.setMousePositionOnWindow(
        core::Position2i { WINDOW_WIDTH<core::Int32> / 2, WINDOW_HEIGHT<core::Int32> / 2 });

    m_scene = std::make_unique<Scene>(*m_engine, *m_window);
    m_engine->setScene(*m_scene);

    auto last_timepoint = Clock::now();
    while (m_window->isOpen()) {
        const auto now_timepoint = Clock::now();
        const auto delta =
            Chrono::duration<float, Chrono::seconds::period> { now_timepoint - last_timepoint }
                .count();
        last_timepoint = now_timepoint;

        m_event_handler->update();

        m_scene->update(delta);

        m_engine->render();
    }

    m_engine->device().waitIdle();
}

void App::doInitWindow() {
    auto video_settings =
        window::VideoSettings { .size = core::Extentu { .width  = WINDOW_WIDTH<core::UInt32>,
                                                        .height = WINDOW_HEIGHT<core::UInt32> } };
    auto window_style = window::WindowStyle::Close;

    if (m_fullscreen) {
        video_settings = window::Window::getDesktopFullscreenSize();
        window_style   = window::WindowStyle::Fullscreen;
    }

    m_window        = std::make_unique<window::Window>(WINDOW_TITLE, video_settings, window_style);
    m_event_handler = std::make_unique<window::EventHandler>(*m_window);

    m_event_handler->addCallback(window::EventType::Closed,
                                 [this]([[maybe_unused]] const auto &event) { m_window->close(); });
    m_event_handler->addCallback(window::EventType::KeyPressed, [this](const auto &event) {
        if (event.key_event.key == window::Key::F1)
            m_scene->toggleDebugGUI();
        if (event.key_event.key == window::Key::F2)
            m_scene->toggleFreezeCamera();
        if (event.key_event.key == window::Key::F3)
            m_scene->toggleMSAA();
        if (event.key_event.key == window::Key::F4)
            m_scene->toggleWireframe();
    });

    m_engine = std::make_unique<engine::Engine>(*m_window, "WaterSimulation");
}
