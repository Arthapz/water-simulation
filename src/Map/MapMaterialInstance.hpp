// Copyright (C) 2019 Arthur LAURENT <arthur.laurent4@gmail.com>
// This file is subject to the license terms in the LICENSE file
// found in the top-level of this distribution

#pragma once

/////////// - StormKit::core - ///////////
#include <storm/core/NonCopyable.hpp>
#include <storm/core/Platform.hpp>

/////////// - StormKit::engine - ///////////
#include <storm/engine/Fwd.hpp>

#include <storm/engine/material/MaterialInstance.hpp>

class MapMaterial;
class MapMaterialInstance final: public storm::engine::MaterialInstance {
      public:
        static constexpr auto GRASS_MAP_NAME = std::string_view { "grass" };
        static constexpr auto DIRT_MAP_NAME = std::string_view { "dirt" };

        MapMaterialInstance(const storm::engine::Scene &scene, const MapMaterial &material);
        virtual ~MapMaterialInstance();

        MapMaterialInstance(MapMaterialInstance &&);
        MapMaterialInstance &operator=(MapMaterialInstance &&);

        inline void setGrassMap(
            const storm::render::Texture &map,
            std::optional<storm::render::Sampler::Settings> sampler_settings = std::nullopt) noexcept;

        inline void setDirtMap(
            const storm::render::Texture &map,
            std::optional<storm::render::Sampler::Settings> sampler_settings = std::nullopt) noexcept;
};

#include "MapMaterialInstance.inl"
