// Copyright (C) 2019 Arthur LAURENT <arthur.laurent4@gmail.com>
// This file is subject to the license terms in the LICENSE file
// found in the top-level of this distribution

#pragma once

/////////// - StormKit::core - ///////////
#include <storm/core/NonCopyable.hpp>
#include <storm/core/Platform.hpp>

/////////// - StormKit::engine - ///////////
#include <storm/engine/Fwd.hpp>

#include <storm/engine/material/MaterialInstance.hpp>

class WaterMaterial;
class WaterMaterialInstance final: public storm::engine::MaterialInstance {
      public:
        WaterMaterialInstance(const storm::engine::Scene &scene, const WaterMaterial &material);
        virtual ~WaterMaterialInstance();

        WaterMaterialInstance(WaterMaterialInstance &&);
        WaterMaterialInstance &operator=(WaterMaterialInstance &&);

};

#include "WaterMaterialInstance.inl"
