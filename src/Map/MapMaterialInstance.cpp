#include "MapMaterial.hpp"
#include "MapMaterialInstance.hpp"

/////////// - StormKit::engine - ///////////
#include <storm/engine/Engine.hpp>

#include <storm/engine/scene/PBRScene.hpp>

using namespace storm;

////////////////////////////////////////
////////////////////////////////////////
MapMaterialInstance::MapMaterialInstance(const engine::Scene &scene,
                                         const MapMaterial &material)
    : engine::MaterialInstance { scene, material } {
    const auto &texture_pool = scene.texturePool();
    const auto &texture      = texture_pool.get("StormKit:BlankTexture:2D");

    setGrassMap(texture);
    setDirtMap(texture);

    flush();
}

////////////////////////////////////////
////////////////////////////////////////
MapMaterialInstance::~MapMaterialInstance() = default;

////////////////////////////////////////
////////////////////////////////////////
MapMaterialInstance::MapMaterialInstance(MapMaterialInstance &&) = default;

////////////////////////////////////////
////////////////////////////////////////
MapMaterialInstance &MapMaterialInstance::operator=(MapMaterialInstance &&) = default;

