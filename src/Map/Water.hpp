#pragma once

#include "WaterMaterial.hpp"
#include "WaterMaterialInstance.hpp"

/////////// - StormKit::engine - ///////////
#include <storm/image/Fwd.hpp>

/////////// - StormKit::render - ///////////
#include <storm/render/Fwd.hpp>

/////////// - StormKit::engine - ///////////
#include <storm/engine/Fwd.hpp>

#include <storm/engine/core/Vertex.hpp>

#include <storm/engine/drawable/Drawable.hpp>

class WaterMaterial;
class WaterMaterialInstance;
class Water final: public storm::engine::Drawable {
  public:
    Water(storm::engine::Engine &engine, const storm::engine::Material &material, storm::core::UInt32 width, storm::core::UInt32 height, float patch_size = 2.f);
    ~Water() override;

    Water(Water &&);
    Water &operator=(Water &&);

    void render(storm::render::CommandBuffer &cmb,
                const storm::render::RenderPass &pass,
                std::vector<storm::engine::BindableBaseConstObserverPtr> bindables,
                storm::render::GraphicsPipelineState state,
                float delta_time = 0.f) override;

    inline WaterMaterialInstance &materialInstance() noexcept { return *m_material_instance; }
    inline const WaterMaterialInstance &materialInstance() const noexcept { return *m_material_instance; };
  protected:
    void recomputeBoundingBox() const noexcept override;

  private:
    storm::engine::VertexArray m_vertex_array;
    storm::render::HardwareBufferOwnedPtr m_vertex_buffer;

    storm::engine::LargeIndexArray m_index_array;
    storm::render::HardwareBufferOwnedPtr m_index_buffer;

    std::unique_ptr<WaterMaterialInstance> m_material_instance;
};
