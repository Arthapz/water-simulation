// Copyright (C) 2019 Arthur LAURENT <arthur.laurent4@gmail.com>
// This file is subject to the license terms in the LICENSE file
// found in the top-level of this distribution

#pragma once

////////////////////////////////////////
////////////////////////////////////////
void MapMaterialInstance::setGrassMap(
    const storm::render::Texture &map,
    std::optional<storm::render::Sampler::Settings> sampler_settings) noexcept {
    auto settings = sampler_settings.value_or(
        storm::render::Sampler::Settings {
                                    .mag_filter        = storm::render::Filter::Nearest,
                                    .min_filter        = storm::render::Filter::Nearest,
                                    .address_mode_u    = storm::render::SamplerAddressMode::Repeat,
                                    .address_mode_v    = storm::render::SamplerAddressMode::Repeat,
                                    .address_mode_w    = storm::render::SamplerAddressMode::Repeat,
                                    .enable_anisotropy = true,
                                    .max_anisotropy    = m_engine->maxAnisotropy(),
                                    .border_color      = storm::render::BorderColor::Float_Opaque_White,
                                    .compare_operation = storm::render::CompareOperation::Never,
                                    .max_lod           = static_cast<float>(map.mipLevels()) });

    setSampledTexture(GRASS_MAP_NAME,
                      map,
                      storm::render::TextureViewType::T2D,
                      std::nullopt,
                      std::move(settings));
}

////////////////////////////////////////
////////////////////////////////////////
void MapMaterialInstance::setDirtMap(
    const storm::render::Texture &map,
    std::optional<storm::render::Sampler::Settings> sampler_settings) noexcept {
    auto settings = sampler_settings.value_or(
        storm::render::Sampler::Settings {
                                    .mag_filter        = storm::render::Filter::Nearest,
                                    .min_filter        = storm::render::Filter::Nearest,
                                    .address_mode_u    = storm::render::SamplerAddressMode::Repeat,
                                    .address_mode_v    = storm::render::SamplerAddressMode::Repeat,
                                    .address_mode_w    = storm::render::SamplerAddressMode::Repeat,
                                    .enable_anisotropy = true,
                                    .max_anisotropy    = m_engine->maxAnisotropy(),
                                    .border_color      = storm::render::BorderColor::Float_Opaque_White,
                                    .compare_operation = storm::render::CompareOperation::Never,
                                    .max_lod           = static_cast<float>(map.mipLevels()) });

    setSampledTexture(DIRT_MAP_NAME,
                      map,
                      storm::render::TextureViewType::T2D,
                      std::nullopt,
                      std::move(settings));
}

