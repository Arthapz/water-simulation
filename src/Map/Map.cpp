// Copyright (C) 2019 Arthur LAURENT <arthur.laurent4@gmail.com>
// This file is subject to the license terms in the LICENSE file
// found in the top-level of this distribution

#include "Map.hpp"

/////////// - StormKit::render - ///////////
#include <storm/render/core/CommandBuffer.hpp>
#include <storm/render/core/Device.hpp>

#include <storm/render/pipeline/PipelineCache.hpp>

#include <storm/render/sync/Fence.hpp>

/////////// - StormKit::image - ///////////
#include <storm/image/Image.hpp>

/////////// - StormKit::engine - ///////////
#include <storm/engine/Engine.hpp>

#include <storm/engine/core/Bindable.hpp>

using namespace storm;

struct Vertex {
    core::Vector3f position;
    core::Vector3f normal;
    core::Vector2f texcoord;
};

static constexpr auto BINDINGS = render::VertexBindingDescriptionStaticArray<1> { { 0u, sizeof(Vertex) } };
static constexpr auto ATTRIBUTES = render::VertexInputAttributeDescriptionStaticArray<3> {
    render::VertexInputAttributeDescription{ 0u, 0u, render::Format::Float3, offsetof(Vertex, position) },
    { 1u, 0u, render::Format::Float3, offsetof(Vertex, normal) },
    { 2u, 0u, render::Format::Float2, offsetof(Vertex, texcoord) },
};

////////////////////////////////////////
////////////////////////////////////////
inline float getHeight(const image::Image &heightmap, core::Vector2u position, core::UInt32 scale = 1) noexcept {
    auto rpos = core::Vector2u{};
    rpos.x = std::max(0u, std::min(position.x, heightmap.extent().width - 1u));
    rpos.y = std::max(0u, std::min(position.y, heightmap.extent().height - 1u));

    return (static_cast<float>(heightmap.pixel({rpos.x, rpos.y, 0u})[0]) / 255.f) * scale;
}

////////////////////////////////////////
////////////////////////////////////////
Map::Map(engine::Engine &engine, const engine::Material &material, const image::Image &heightmap, float patch_size)
    : engine::Drawable{engine} {
    const auto &device         = m_engine->device();

    const auto &extent = heightmap.extent();

    const auto vertex_count = extent.width * extent.height * 4;
    const auto index_count = (extent.width - 1u) * (extent.height - 1u) * 6;

    m_vertex_array.resize(vertex_count * sizeof(Vertex));
    m_index_array.resize(index_count);

    constexpr auto scale = 255.f;

    for(auto x = 0u;x < extent.width; ++x) {
        for(auto y = 0u; y < extent.height; ++y) {
            auto vertex_index = x + y * extent.width;
            auto &v1 = m_vertex_array.at<Vertex>(vertex_index);
            //auto &v2 = m_vertex_array.at<Vertex>(vertex_index);
            //auto &v3 = m_vertex_array.at<Vertex>(vertex_index);
            //auto &v4 = m_vertex_array.at<Vertex>(vertex_index);

            v1.position = {
                   x * patch_size + patch_size / 2.f - static_cast<float>(extent.width) * patch_size / 2.f,
                   getHeight(heightmap, {x, y}, scale),
                   y * patch_size + patch_size / 2.f - static_cast<float>(extent.height) * patch_size / 2.f,
            },
            v1.texcoord = {static_cast<float>(x) / static_cast<float>(patch_size),
                           static_cast<float>(y) / static_cast<float>(patch_size)};

            /*v2.position = {
                   (x + 1.f) * patch_size + patch_size / 2.f - static_cast<float>(extent.width) * patch_size / 2.f,
                   getHeight(heightmap, {x, y}, scale) - 10.f,
                   y * patch_size + patch_size / 2.f - static_cast<float>(extent.height) * patch_size / 2.f,
            },
            v2.texcoord = {static_cast<float>(x + 1) / static_cast<float>(patch_size),
                           static_cast<float>(y) / static_cast<float>(patch_size)};

            v3.position = {
                   x * patch_size + patch_size / 2.f - static_cast<float>(extent.width) * patch_size / 2.f,
                   getHeight(heightmap, {x, y}, scale) - 10.f,
                   (y + 1.f) * patch_size + patch_size / 2.f - static_cast<float>(extent.height) * patch_size / 2.f,
            },
            v3.texcoord = {static_cast<float>(x) / static_cast<float>(patch_size),
                           static_cast<float>(y + 1) / static_cast<float>(patch_size)};

            v4.position = {
                   (x + 1.f) * patch_size + patch_size / 2.f - static_cast<float>(extent.width) * patch_size / 2.f,
                   getHeight(heightmap, {x, y}, scale) - 10.f,
                   (y + 1.f) * patch_size + patch_size / 2.f - static_cast<float>(extent.height) * patch_size / 2.f,
            },
            v4.texcoord = {static_cast<float>(x + 1) / static_cast<float>(patch_size),
                           static_cast<float>(y + 1) / static_cast<float>(patch_size)};*/
        }
    }

    for(auto y = 0u;y < extent.height; ++y) {
        for(auto x = 0u; x < extent.width; ++x) {
            auto &vertex = m_vertex_array.at<Vertex>(x + y * extent.width);

            auto dx = getHeight(heightmap, {x < extent.width - 1u ? x + 1u : x, y}, scale) - getHeight(heightmap, {x > 0u ? x - 1u : x, y}, scale);
            if(x == 0u || x == extent.width - 1u)
                dx *= 2.f;

            auto dy = getHeight(heightmap, {x, y < extent.height - 1u ? y + 1u : y}, scale) - getHeight(heightmap, {x, y > 0u ? y - 1u : y}, scale);
            if(y == 0u || y == extent.height - 1u)
                dy *= 2.f;

            const auto A = core::Vector3f(1.f, 0.f, dx);
            const auto B = core::Vector3f(0.f, 1.f, dy);

            const auto normal = (core::normalize(core::cross(A, B)) + 1.f) * 0.5f;

            vertex.normal = std::move(normal);
        }
    }

    for(auto x = 0u;x < extent.width - 1u; ++x) {
        for(auto y = 0u; y < extent.height - 1u; ++y) {
            auto index = (x + y * (extent.width - 1u)) * 6;
            m_index_array[index] = x + y * extent.width;
            m_index_array[index + 1] = m_index_array[index] + extent.width;
            m_index_array[index + 2] = m_index_array[index + 1] + 1;

            m_index_array[index + 3] = m_index_array[index + 1] + 1;
            m_index_array[index + 4] = m_index_array[index] + 1;
            m_index_array[index + 5] = m_index_array[index];
        }
    }

    const auto vertex_buffer_size = std::size(m_vertex_array);
    const auto index_buffer_size = std::size(m_index_array) * sizeof(core::UInt32);

    auto staging_buffer = device.createStagingBuffer(vertex_buffer_size + index_buffer_size);
    staging_buffer.upload<core::Byte>(m_vertex_array);
    staging_buffer.upload<core::UInt32>(m_index_array, vertex_buffer_size);

    m_vertex_buffer = device.createVertexBufferPtr(vertex_buffer_size,
                                                   render::MemoryProperty::Device_Local,
                                                   true);
    m_index_buffer = device.createIndexBufferPtr(index_buffer_size,
                                                 render::MemoryProperty::Device_Local,
                                                 true);

    auto cmb = device.graphicsQueue().createCommandBuffer();
    cmb.begin(true);
    cmb.copyBuffer(staging_buffer, *m_vertex_buffer, vertex_buffer_size);
    cmb.copyBuffer(staging_buffer, *m_index_buffer, index_buffer_size, vertex_buffer_size);
    cmb.end();

    cmb.build();

    auto fence = device.createFence();
    cmb.submit({}, {}, core::makeObserver(fence));

    fence.wait();

    auto instance = material.createInstancePtr();
    m_material_instance.reset(static_cast<MapMaterialInstance*>(instance.release()));
}

////////////////////////////////////////
////////////////////////////////////////
Map::~Map() = default;

////////////////////////////////////////
////////////////////////////////////////
Map::Map(Map &&) = default;

////////////////////////////////////////
////////////////////////////////////////
Map &Map::operator=(Map &&) = default;

////////////////////////////////////////
////////////////////////////////////////
void Map::render(render::CommandBuffer &cmb,
                 const render::RenderPass &pass,
                 std::vector<engine::BindableBaseConstObserverPtr> bindables,
                 render::GraphicsPipelineState state,
                 [[maybe_unused]] float delta_time) {
    m_material_instance->flush();

    auto push_constant_data = core::ByteArray{};
    push_constant_data.resize(sizeof(m_clip_plane));

    std::memcpy(std::data(push_constant_data), reinterpret_cast<const core::Byte *>(&m_clip_plane), sizeof(m_clip_plane));

    bindables.emplace_back(core::makeConstObserver(m_material_instance));

    state.shader_state = m_material_instance->parent().data().shader_state;
    state.vertex_input_state.binding_descriptions = BINDINGS;
    state.vertex_input_state.input_attribute_descriptions = ATTRIBUTES;
    state.rasterization_state.cull_mode = render::CullMode::None;

    auto descriptors = std::vector<render::DescriptorSetCRef> {};
    auto offsets     = std::vector<core::UOffset> {};

    descriptors.reserve(std::size(bindables));
    offsets.reserve(std::size(bindables));

    for (const auto &bindable : bindables) {
        descriptors.push_back(bindable->descriptorSet());

        const auto &offset = bindable->offset();
        if (offset.has_value()) offsets.push_back(offset.value());

        state.layout.descriptor_set_layouts.emplace_back(
            core::makeConstObserver(bindable->descriptorLayout()));
    }

    state.layout.push_constant_ranges.emplace_back(render::PushConstantRange{ .stages = render::ShaderStage::Vertex, .offset = 0u, .size = sizeof(m_clip_plane)});

    const auto &pipeline = m_engine->pipelineCache().getPipeline(state, pass);

    cmb.bindVertexBuffers({ *m_vertex_buffer}, {0u});
    cmb.bindIndexBuffer(*m_index_buffer, 0u, true);
    cmb.bindGraphicsPipeline(pipeline);
    cmb.pushConstants(pipeline, render::ShaderStage::Vertex, push_constant_data);
    cmb.bindDescriptorSets(pipeline, std::move(descriptors), std::move(offsets));

    cmb.drawIndexed(std::size(m_index_array));
}

////////////////////////////////////////
////////////////////////////////////////
void Map::recomputeBoundingBox() const noexcept
{

}
