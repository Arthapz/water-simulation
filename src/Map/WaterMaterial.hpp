#pragma once

/////////// - StormKit::core - ///////////
#include <storm/core/Hash.hpp>
#include <storm/core/NonCopyable.hpp>
#include <storm/core/Platform.hpp>

/////////// - StormKit::engine - ///////////
#include <storm/engine/Fwd.hpp>

#include <storm/engine/material/Material.hpp>

class WaterMaterial final: public storm::engine::Material {
   public:
        static inline const auto WATER_VERT_SHADER_NAME =
            std::string { "WaterMaterial:Vert" };
        static inline const auto WATER_FRAG_SHADER_NAME =
            std::string { "WaterMaterial:Frag" };

        explicit WaterMaterial(storm::engine::Scene &scene);
        virtual ~WaterMaterial();

        WaterMaterial(WaterMaterial &&);
        WaterMaterial &operator=(WaterMaterial &&);

        [[nodiscard]] storm::engine::MaterialInstanceOwnedPtr createInstancePtr() const noexcept override;

      private:
        void buildShaders();
    };

HASH_FUNC(WaterMaterial)
