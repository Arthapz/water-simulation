#pragma once

#include "MapMaterial.hpp"
#include "MapMaterialInstance.hpp"

/////////// - StormKit::engine - ///////////
#include <storm/image/Fwd.hpp>

/////////// - StormKit::render - ///////////
#include <storm/render/Fwd.hpp>

/////////// - StormKit::engine - ///////////
#include <storm/engine/Fwd.hpp>

#include <storm/engine/core/Vertex.hpp>

#include <storm/engine/drawable/Drawable.hpp>

class MapMaterial;
class MapMaterialInstance;
class Map final: public storm::engine::Drawable {
  public:
    Map(storm::engine::Engine &engine, const storm::engine::Material &material, const storm::image::Image &heightmap, float patch_size = 2.f);
    ~Map() override;

    Map(Map &&);
    Map &operator=(Map &&);

    void render(storm::render::CommandBuffer &cmb,
                const storm::render::RenderPass &pass,
                std::vector<storm::engine::BindableBaseConstObserverPtr> bindables,
                storm::render::GraphicsPipelineState state,
                float delta_time = 0.f) override;

    inline void setClipPlane(storm::core::Vector3f clip_plane) { m_clip_plane = std::move(clip_plane); }

    inline MapMaterialInstance &materialInstance() noexcept { return *m_material_instance; }
    inline const MapMaterialInstance &materialInstance() const noexcept { return *m_material_instance; };
  protected:
    void recomputeBoundingBox() const noexcept override;

  private:
    storm::engine::VertexArray m_vertex_array;
    storm::render::HardwareBufferOwnedPtr m_vertex_buffer;

    storm::engine::LargeIndexArray m_index_array;
    storm::render::HardwareBufferOwnedPtr m_index_buffer;

    storm::core::Vector3f m_clip_plane = {0.f, 0.f, 0.f};

    std::unique_ptr<MapMaterialInstance> m_material_instance;
};
