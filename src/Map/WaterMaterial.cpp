// Copyright (C) 2019 Arthur LAURENT <arthur.laurent4@gmail.com>
// This file is subject to the license terms in the LICENSE file
// found in the top-level of this distribution

#include "WaterMaterial.hpp"
#include "WaterMaterialInstance.hpp"

/////////// - StormKit::device - ///////////
#include <storm/render/core/CommandBuffer.hpp>
#include <storm/render/core/Queue.hpp>

#include <storm/render/sync/Fence.hpp>

/////////// - StormKit::engine - ///////////
#include <storm/engine/Engine.hpp>

#include <storm/engine/scene/PBRScene.hpp>

using namespace storm;

static const auto WATER_VERT_SHADER_DATA = std::vector<core::UInt32> {
#include "Water.vert.spv.hpp"
};
static const auto WATER_FRAG_SHADER_DATA = std::vector<core::UInt32> {
#include "Water.frag.spv.hpp"
};

WaterMaterial::WaterMaterial(engine::Scene &scene) : engine::Material { scene } {
    buildShaders();

    const auto &shader_pool     = m_scene->shaderPool();
    const auto &vertex_shader   = shader_pool.get(WATER_VERT_SHADER_NAME);
    const auto &fragment_shader = shader_pool.get(WATER_FRAG_SHADER_NAME);

    addShader(vertex_shader);
    addShader(fragment_shader);

    finalize();
}

////////////////////////////////////////
////////////////////////////////////////
WaterMaterial::~WaterMaterial() = default;

////////////////////////////////////////
////////////////////////////////////////
WaterMaterial::WaterMaterial(WaterMaterial &&) = default;

////////////////////////////////////////
////////////////////////////////////////
WaterMaterial &WaterMaterial::operator=(WaterMaterial &&) = default;

////////////////////////////////////////
////////////////////////////////////////
engine::MaterialInstanceOwnedPtr WaterMaterial::createInstancePtr() const noexcept {
    return std::make_unique<WaterMaterialInstance>(*m_scene, *this);
}

////////////////////////////////////////
////////////////////////////////////////
void WaterMaterial::buildShaders() {
    auto &shader_pool = m_scene->shaderPool();
    if (!shader_pool.has(WATER_VERT_SHADER_NAME)) {
        shader_pool.create(WATER_VERT_SHADER_NAME,
                           WATER_VERT_SHADER_DATA,
                           render::ShaderStage::Vertex,
                           m_scene->engine().device());
    }

    if (!shader_pool.has(WATER_FRAG_SHADER_NAME)) {
        shader_pool.create(WATER_FRAG_SHADER_NAME,
                           WATER_FRAG_SHADER_DATA,
                           render::ShaderStage::Fragment,
                           m_scene->engine().device());
    }
}
