// Copyright (C) 2019 Arthur LAURENT <arthur.laurent4@gmail.com>
// This file is subject to the license terms in the LICENSE file
// found in the top-level of this distribution

#include "MapMaterial.hpp"
#include "MapMaterialInstance.hpp"

/////////// - StormKit::device - ///////////
#include <storm/render/core/CommandBuffer.hpp>
#include <storm/render/core/Queue.hpp>

#include <storm/render/sync/Fence.hpp>

/////////// - StormKit::engine - ///////////
#include <storm/engine/Engine.hpp>

#include <storm/engine/scene/PBRScene.hpp>

using namespace storm;

static const auto MAP_VERT_SHADER_DATA = std::vector<core::UInt32> {
#include "Map.vert.spv.hpp"
};
static const auto MAP_FRAG_SHADER_DATA = std::vector<core::UInt32> {
#include "Map.frag.spv.hpp"
};

MapMaterial::MapMaterial(engine::Scene &scene) : engine::Material { scene } {
    buildShaders();

    const auto &shader_pool     = m_scene->shaderPool();
    const auto &vertex_shader   = shader_pool.get(MAP_VERT_SHADER_NAME);
    const auto &fragment_shader = shader_pool.get(MAP_FRAG_SHADER_NAME);

    addShader(vertex_shader);
    addShader(fragment_shader);

    addSampler(0,
               std::string { MapMaterialInstance::GRASS_MAP_NAME },
               render::TextureViewType::T2D);
    addSampler(1,
               std::string { MapMaterialInstance::DIRT_MAP_NAME },
               render::TextureViewType::T2D);

    finalize();
}

////////////////////////////////////////
////////////////////////////////////////
MapMaterial::~MapMaterial() = default;

////////////////////////////////////////
////////////////////////////////////////
MapMaterial::MapMaterial(MapMaterial &&) = default;

////////////////////////////////////////
////////////////////////////////////////
MapMaterial &MapMaterial::operator=(MapMaterial &&) = default;

////////////////////////////////////////
////////////////////////////////////////
engine::MaterialInstanceOwnedPtr MapMaterial::createInstancePtr() const noexcept {
    return std::make_unique<MapMaterialInstance>(*m_scene, *this);
}

////////////////////////////////////////
////////////////////////////////////////
void MapMaterial::buildShaders() {
    auto &shader_pool = m_scene->shaderPool();
    if (!shader_pool.has(MAP_VERT_SHADER_NAME)) {
        shader_pool.create(MAP_VERT_SHADER_NAME,
                           MAP_VERT_SHADER_DATA,
                           render::ShaderStage::Vertex,
                           m_scene->engine().device());
    }

    if (!shader_pool.has(MAP_FRAG_SHADER_NAME)) {
        shader_pool.create(MAP_FRAG_SHADER_NAME,
                           MAP_FRAG_SHADER_DATA,
                           render::ShaderStage::Fragment,
                           m_scene->engine().device());
    }
}
