// Copyright (C) 2019 Arthur LAURENT <arthur.laurent4@gmail.com>
// This file is subject to the license terms in the LICENSE file
// found in the top-level of this distribution

#include "Water.hpp"

/////////// - StormKit::render - ///////////
#include <storm/render/core/CommandBuffer.hpp>
#include <storm/render/core/Device.hpp>

#include <storm/render/pipeline/PipelineCache.hpp>

#include <storm/render/sync/Fence.hpp>

/////////// - StormKit::image - ///////////
#include <storm/image/Image.hpp>

/////////// - StormKit::engine - ///////////
#include <storm/engine/Engine.hpp>

#include <storm/engine/core/Bindable.hpp>

using namespace storm;

struct Vertex {
    core::Vector3f position;
    core::Vector3f normal;
    core::Vector2f texcoord;
};

static constexpr auto BINDINGS = render::VertexBindingDescriptionStaticArray<1> { { 0u, sizeof(Vertex) } };
static constexpr auto ATTRIBUTES = render::VertexInputAttributeDescriptionStaticArray<3> {
    render::VertexInputAttributeDescription{ 0u, 0u, render::Format::Float3, offsetof(Vertex, position) },
    { 1u, 0u, render::Format::Float3, offsetof(Vertex, normal) },
    { 2u, 0u, render::Format::Float2, offsetof(Vertex, texcoord) },
};

////////////////////////////////////////
////////////////////////////////////////
Water::Water(engine::Engine &engine, const engine::Material &material, core::UInt32 width, core::UInt32 height, float patch_size)
    : engine::Drawable{engine} {
    const auto &device         = m_engine->device();

    const auto &extent = core::Extentu{width, height};

    const auto vertex_count = extent.width * extent.height * 4;
    const auto index_count = (extent.width - 1u) * (extent.height - 1u) * 6;

    m_vertex_array.resize(vertex_count * sizeof(Vertex));
    m_index_array.resize(index_count);

    for(auto x = 0u;x < extent.width; ++x) {
        for(auto y = 0u; y < extent.height; ++y) {
            const auto vertex_index = x + y * extent.width;
            auto &vertex = m_vertex_array.at<Vertex>(vertex_index);

            vertex.position = {
                   x * patch_size + patch_size / 2.f - static_cast<float>(extent.width) * patch_size / 2.f,
                   20.f,
                   y * patch_size + patch_size / 2.f - static_cast<float>(extent.height) * patch_size / 2.f,
            },
            vertex.normal = {0.f, 1.f, 0.f};
            vertex.texcoord = {static_cast<float>(x) / static_cast<float>(patch_size),
                           static_cast<float>(y) / static_cast<float>(patch_size)};
        }
    }

    for(auto x = 0u;x < extent.width - 1u; ++x) {
        for(auto y = 0u; y < extent.height - 1u; ++y) {
            auto index = (x + y * (extent.width - 1u)) * 6;
            m_index_array[index] = x + y * extent.width;
            m_index_array[index + 1] = m_index_array[index] + extent.width;
            m_index_array[index + 2] = m_index_array[index + 1] + 1;

            m_index_array[index + 3] = m_index_array[index + 1] + 1;
            m_index_array[index + 4] = m_index_array[index] + 1;
            m_index_array[index + 5] = m_index_array[index];
        }
    }

    const auto vertex_buffer_size = std::size(m_vertex_array);
    const auto index_buffer_size = std::size(m_index_array) * sizeof(core::UInt32);

    auto staging_buffer = device.createStagingBuffer(vertex_buffer_size + index_buffer_size);
    staging_buffer.upload<core::Byte>(m_vertex_array);
    staging_buffer.upload<core::UInt32>(m_index_array, vertex_buffer_size);

    m_vertex_buffer = device.createVertexBufferPtr(vertex_buffer_size,
                                                   render::MemoryProperty::Device_Local,
                                                   true);
    m_index_buffer = device.createIndexBufferPtr(index_buffer_size,
                                                 render::MemoryProperty::Device_Local,
                                                 true);

    auto cmb = device.graphicsQueue().createCommandBuffer();
    cmb.begin(true);
    cmb.copyBuffer(staging_buffer, *m_vertex_buffer, vertex_buffer_size);
    cmb.copyBuffer(staging_buffer, *m_index_buffer, index_buffer_size, vertex_buffer_size);
    cmb.end();

    cmb.build();

    auto fence = device.createFence();
    cmb.submit({}, {}, core::makeObserver(fence));

    fence.wait();

    auto instance = material.createInstancePtr();
    m_material_instance.reset(static_cast<WaterMaterialInstance*>(instance.release()));
}

////////////////////////////////////////
////////////////////////////////////////
Water::~Water() = default;

////////////////////////////////////////
////////////////////////////////////////
Water::Water(Water &&) = default;

////////////////////////////////////////
////////////////////////////////////////
Water &Water::operator=(Water &&) = default;

////////////////////////////////////////
////////////////////////////////////////
void Water::render(render::CommandBuffer &cmb,
                 const render::RenderPass &pass,
                 std::vector<engine::BindableBaseConstObserverPtr> bindables,
                 render::GraphicsPipelineState state,
                 [[maybe_unused]] float delta_time) {
    m_material_instance->flush();

    bindables.emplace_back(core::makeConstObserver(m_material_instance));

    state.shader_state = m_material_instance->parent().data().shader_state;
    state.vertex_input_state.binding_descriptions = BINDINGS;
    state.vertex_input_state.input_attribute_descriptions = ATTRIBUTES;
    state.rasterization_state.cull_mode = render::CullMode::None;

    auto descriptors = std::vector<render::DescriptorSetCRef> {};
    auto offsets     = std::vector<core::UOffset> {};

    descriptors.reserve(std::size(bindables));
    offsets.reserve(std::size(bindables));

    for (const auto &bindable : bindables) {
        descriptors.push_back(bindable->descriptorSet());

        const auto &offset = bindable->offset();
        if (offset.has_value()) offsets.push_back(offset.value());

        state.layout.descriptor_set_layouts.emplace_back(
            core::makeConstObserver(bindable->descriptorLayout()));
    }

    const auto &pipeline = m_engine->pipelineCache().getPipeline(state, pass);

    cmb.bindVertexBuffers({ *m_vertex_buffer}, {0u});
    cmb.bindIndexBuffer(*m_index_buffer, 0u, true);
    cmb.bindGraphicsPipeline(pipeline);
    cmb.bindDescriptorSets(pipeline, std::move(descriptors), std::move(offsets));

    cmb.drawIndexed(std::size(m_index_array));
}

////////////////////////////////////////
////////////////////////////////////////
void Water::recomputeBoundingBox() const noexcept
{

}
