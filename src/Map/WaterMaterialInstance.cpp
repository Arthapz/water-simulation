#include "WaterMaterial.hpp"
#include "WaterMaterialInstance.hpp"

/////////// - StormKit::engine - ///////////
#include <storm/engine/Engine.hpp>

#include <storm/engine/scene/PBRScene.hpp>

using namespace storm;

////////////////////////////////////////
////////////////////////////////////////
WaterMaterialInstance::WaterMaterialInstance(const engine::Scene &scene,
                                         const WaterMaterial &material)
    : engine::MaterialInstance { scene, material } {

    flush();
}

////////////////////////////////////////
////////////////////////////////////////
WaterMaterialInstance::~WaterMaterialInstance() = default;

////////////////////////////////////////
////////////////////////////////////////
WaterMaterialInstance::WaterMaterialInstance(WaterMaterialInstance &&) = default;

////////////////////////////////////////
////////////////////////////////////////
WaterMaterialInstance &WaterMaterialInstance::operator=(WaterMaterialInstance &&) = default;

