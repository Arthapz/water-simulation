#pragma once

/////////// - StormKit::core - ///////////
#include <storm/core/Hash.hpp>
#include <storm/core/NonCopyable.hpp>
#include <storm/core/Platform.hpp>

/////////// - StormKit::engine - ///////////
#include <storm/engine/Fwd.hpp>

#include <storm/engine/material/Material.hpp>

class MapMaterial final: public storm::engine::Material {
   public:
        static inline const auto MAP_VERT_SHADER_NAME =
            std::string { "MapMaterial:Vert" };
        static inline const auto MAP_FRAG_SHADER_NAME =
            std::string { "MapMaterial:Frag" };

        explicit MapMaterial(storm::engine::Scene &scene);
        virtual ~MapMaterial();

        MapMaterial(MapMaterial &&);
        MapMaterial &operator=(MapMaterial &&);

        [[nodiscard]] storm::engine::MaterialInstanceOwnedPtr createInstancePtr() const noexcept override;

      private:
        void buildShaders();
    };

HASH_FUNC(MapMaterial)
