// Copyright (C) 2019 Arthur LAURENT <arthur.laurent4@gmail.com>
// This file is subject to the license terms in the LICENSE file
// found in the top-level of this distribution

#include <storm/log/FileLogger.hpp>
#include <storm/log/LogHandler.hpp>

#include "App.hpp"

#include <storm/main/Main.hpp>

int main(int argc, char **argv) {
	using namespace storm;

	// log::LogHandler::setupLogger<log::FileLogger>("log.txt");
	log::LogHandler::setupDefaultLogger();

	auto app = App {};
	app.run(argc, argv);

	return EXIT_SUCCESS;
}
