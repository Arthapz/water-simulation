#version 460 core
#extension GL_ARB_separate_shader_objects : enable
#pragma shader_stage(vertex)

layout(location = 0) in vec3 vertex_position;
layout(location = 1) in vec3 vertex_normal;
layout(location = 2) in vec2 vertex_texcoord;

layout(location = 0) out vec3 out_position;
layout(location = 1) out vec3 out_normal;
layout(location = 2) out vec2 out_texcoord;

layout(set = 0, binding = 0, std140) uniform Camera {
    vec4 position;
    mat4 projection;
    mat4 view;
} camera;

out gl_PerVertex  {
    vec4 gl_Position;
};

void main() {
    out_position = vertex_position;
    out_normal = vertex_normal;
    out_texcoord = vertex_texcoord;

    gl_Position = camera.projection * camera.view * vec4(vertex_position, 1.f);
}
