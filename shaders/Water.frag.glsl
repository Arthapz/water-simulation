#version 460 core
#extension GL_ARB_separate_shader_objects : enable
#pragma shader_stage(fragment)

layout(location = 0) in vec3 in_position;
layout(location = 1) in vec3 in_normal;
layout(location = 2) in vec2 in_texcoord;

layout(location = 0) out vec4 out_color;

layout(set = 0, binding = 0, std140) uniform Camera {
    vec4 position;
    mat4 projection;
    mat4 view;
} camera;

vec4 sRGBtoLinear(vec4 srgb_in);

const vec3 light_color     = vec3(1.f, 1.f, 1.f);
const vec3 light_direction = vec3(0,
                                  0,
                                  2.f);
const float blend_limit = 35.f;

void main() {
    vec4 albedo = vec4(0.f, 0.f, 1.f, 1.f);

    vec3 n = normalize(in_normal);
    vec3 l = normalize(light_direction.xyz);

    float NdotL = clamp(dot(n, l), 0.001f, 1.f);

    vec3 ambiant = vec3(0.5f);
    vec3 diffuse = vec3(NdotL);

    out_color = vec4((ambiant + diffuse) * albedo.rgb, 1.f);
}

vec4 sRGBtoLinear(vec4 srgb_in) {
    vec3 lin_out = pow(srgb_in.rgb, vec3(2.2));

    return srgb_in;
}
